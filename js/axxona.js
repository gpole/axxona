(function ($) {
    "use strict"; // Start of use strict
    // ===== Scroll to Top ==== 
    $(window).scroll(function () {
        if ($(this).scrollTop() >= 250) { // If page is scrolled more than 150px
            $('#return-to-top').fadeIn(200); // Fade in the arrow
        } else {
            $('#return-to-top').fadeOut(200); // Else fade out the arrow
        }
    });
    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('.return-to-top').bind('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

})(jQuery); // End of use strict


$(function () {
    $('[data-toggle="popover"]').popover()
});


$(document).ready(function () {
    resizeDiv();
});

window.onresize = function (event) {
    resizeDiv();
}

function resizeDiv() {
    vph = $(window).height();
    nvh = $('.hero').height();
    $('#heading').css({
        'height': vph - nvh
    });
}

